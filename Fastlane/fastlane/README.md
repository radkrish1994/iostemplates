fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios add_UDID
```
fastlane ios add_UDID
```
To add UDID
### ios generate_provision
```
fastlane ios generate_provision
```
Description of what the lane does
### ios resign_ipa
```
fastlane ios resign_ipa
```
Description of what the lane does
### ios custom_lane
```
fastlane ios custom_lane
```
Description of what the lane does
### ios build_project
```
fastlane ios build_project
```
create build after installing cocoapods and incrementing build no.
### ios build_ipa
```
fastlane ios build_ipa
```
Build project - for any scheme
### ios xctest_case
```
fastlane ios xctest_case
```
xctest cases running
### ios test
```
fastlane ios test
```
Env Test - for testing Environment variables.

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
